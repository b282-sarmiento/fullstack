fetch('https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((json) => console.log(json));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: JSON.stringify({
      title: document.querySelector('#txt-title').value,
      body: document.querySelector('#txt-body').value,
      userId: 1
    })
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert('Successfully added!');
      document.querySelector('#txt-title').value = '';
      document.querySelector('#txt-body').value = '';
    });
});

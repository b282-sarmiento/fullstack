// import {Row, Col, Button} from 'react-bootstrap'
// import { useNavigate } from 'react-router-dom';
// export default function NotFound() {
//     const navigate = useNavigate();

//     const HomePage = () => {
//         navigate('/');
//     };
// return (
//     <>
//     <Row>
//     	<Col className="p-5">
//             <h1>Error 404- Page not found</h1>
//             <p>the page you are looking for cannot be found.</p>
//             <Button variant="primary" onClick= {HomePage}>Back to Home</Button>
//         </Col>
//     </Row>
    
//     </>
    
// 	)

// }
import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} />
    )
}
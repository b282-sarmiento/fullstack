function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (typeof letter !== "string" || letter.length !== 1) {
        return undefined; // Invalid letter input
    }

    // Convert both the letter and sentence to lowercase for case-insensitive comparison
    const lowercaseLetter = letter.toLowerCase();
    const lowercaseSentence = sentence.toLowerCase();

    // Count how many times the letter has occurred in the given sentence
    for (let i = 0; i < lowercaseSentence.length; i++) {
        if (lowercaseSentence[i] === lowercaseLetter) {
            result++;
        }
    }

    return result;
}



function isIsogram(text) {
    // Convert text to lowercase for case-insensitive comparison
    const lowercaseText = text.toLowerCase();

    // Create an array to store encountered letters
    const encounteredLetters = [];

    // Loop through each character in the text
    for (let i = 0; i < lowercaseText.length; i++) {
        const char = lowercaseText[i];

        // Check if the character is a letter (ignore non-letter characters)
        if (/[a-z]/.test(char)) {
            // If the letter is already encountered, it's not an isogram
            if (encounteredLetters.includes(char)) {
                return false;
            } else {
                // Otherwise, add it to the encounteredLetters array
                encounteredLetters.push(char);
            }
        }
    }

    // If the loop completes without finding any repeating letters, it's an isogram
    return true;
}


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21) {
        // Apply 20% discount for students aged 13 to 21
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else if (age >= 65) {
        // Apply 20% discount for senior citizens (age 65 and above)
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else {
        // Round off the price for people aged 22 to 64
        const roundedPrice = price.toFixed(2);
        return roundedPrice;
    }
}
function findHotCategories(items) {
    const hotCategories = [];

    for (const item of items) {
        if (item.stocks === 0 && !hotCategories.includes(item.category)) {
            hotCategories.push(item.category);
        }
    }

    return hotCategories;
}

// Test data
const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

const hotCategories = findHotCategories(items);
console.log(hotCategories); // Output: ['toiletries', 'gadgets']

function findFlyingVoters(candidateA, candidateB) {
    const commonVoters = [];

    for (const voter of candidateA) {
        if (candidateB.includes(voter)) {
            commonVoters.push(voter);
        }
    }

    return commonVoters;
}

// Test data
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const commonVoters = findFlyingVoters(candidateA, candidateB);
console.log(commonVoters); 

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};